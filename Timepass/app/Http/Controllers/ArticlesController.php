<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function index(){
        $articles = Article::latest('updated_at')->paginate(3);
        return view('articles.index',[
            'articles' => $articles
        ]);
    }

    public function show($article_id)
    {
        $article = Article::findOrFail($article_id);
        return view('articles.show',[
            'article' => $article
        ]);
    }

    public function create(){
        return view('articles.create');
    }

    public function edit($article_id){
        $article = Article::findOrFail($article_id);
        return view('articles.edit',[
            'article' =>$article
        ]);
    }

    public function store()
    {
        // dd(request()->all());
        // dd(request('title'));
        // dd(request()->input('title'));
        $article = new Article();
        $article->title = request()->input('title');
        $article->excerpt = request()->input('excerpt');
        $article->body = request()->input('body');
        $article->save();
        return redirect('/articles');
    }
    public function update($article_id)
    {
        // dd(request()->all())    
        $article = Article::findOrFail($article_id);
        $article->title=request()->input('title');
        $article->excerpt=request()->input('excerpt');
        $article->body=request()->input('body');
        $article->save();
        return redirect('/articles/'.$article->id);
    }
}
