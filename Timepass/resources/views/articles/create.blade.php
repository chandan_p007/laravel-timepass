@extends('layouts.app')

@section('main-content')
<!-- TITLE-->
    <h1 class="mt-4">Create Article</h1>


    <form action="/articles" method="POST">
        
        @csrf
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>

        <div class="form-group">
            <label for="excerpt">Excerpt</label>
            <input type="text" class="form-control" id="excerpt" name="excerpt">
        </div>

        <div class="form-group">
            <label for="body">Body</label>
            <textarea name="body" id="body"  rows="5" class="form-control"></textarea>
        </div>

        <div class="form-group">
            <input type="submit" value="Submit" class="btn btn-outline-primary">
        </div>
        </div>
    </form>
@endsection