@extends('layouts.app')

@section('main-content')
<!-- TITLE-->
    <h1 class="mt-4">Edit Article</h1>


<form action="/articles/{{$article->id}}" method="POST">
        
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title"
        value="{{$article->title}}">
        </div>

        <div class="form-group">
            <label for="excerpt">Excerpt</label>
            <input type="text" class="form-control" id="excerpt" name="excerpt"
            value="{{$article->excerpt}}">
        </div>

        <div class="form-group">
            <label for="body">Body</label>
            <textarea name="body" id="body"  rows="5" class="form-control">
                {{$article->body}}
            </textarea>
        </div>

        <div class="form-group">
            <input type="submit" value="Submit" class="btn btn-outline-primary">
        </div>
        </div>
    </form>
@endsection