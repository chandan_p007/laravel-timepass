@extends('layouts.app')

@section('title','Blog!')
    
@section('main-content')
<h1 class="my-4">Page Heading
    <small>Secondary Text</small>
  </h1>

  <!-- Blog Post -->
  @foreach($articles as $article) 

  <div class="card mb-4">
    <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap">
    <div class="card-body">
    <h2 class="card-title">{{ $article->title }}</h2>
      <p class="card-text">{{ $article->excerpt }}</p>
    <a href="{{"/articles/{$article->id}"}}" class="btn btn-primary">Read More &rarr;</a>
    </div>
    <div class="card-footer text-muted">
      Posted on January 1, 2017 by
      <a href="#">Start Bootstrap</a>
    </div>
  </div>
  @endforeach


  <!-- Pagination -->
   {{ $articles->links() }}
@endsection