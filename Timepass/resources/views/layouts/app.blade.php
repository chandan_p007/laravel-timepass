<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title')</title>

  <!-- Bootstrap core CSS -->
<link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{ asset('css/blog-home.css') }}" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  @include('layouts.partials._navigation')

  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <!-- Blog Entries Column -->
      <div class="col-md-8">
        @yield('main-content')
      </div>
      <!--End of main content -->

      <!-- Sidebar Widgets Column -->
      <div class="col-md-4">
      @if(!Request::is('articles/create'))
        @include('layouts.partials._sidebar')
      @endif
      </div>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  @include('layouts.partials._footer')

  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootsrap.bundle.min.js') }}"></script>

  

</body>

</html>
