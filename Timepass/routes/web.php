<?php

use App\Article;
use App\Http\Controllers\ArticlesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',function(){
    return view('welcome');
});
Route::get('/articles','ArticlesController@index');
Route::get('/articles/create' , 'ArticlesController@create');
Route::get('/articles/{article_id}' , 'ArticlesController@show');
Route::post('/articles' , 'ArticlesController@store');
Route::get('/articles/{article_id}/edit' , 'ArticlesController@edit');
Route::put('/articles/{article_id}' , 'ArticlesController@update');

/**
 * GET POST PUT PATCH DELETE 5 HTTP METHODS
 * 
 * GET: /articles - "To Load ALl articles" 
 * GET: /articles/:id - "To load Article having this id"
 * DELETE: /articles/:id/delete
 * GET: /articles/:id/update
 * PUT: /articles/:id
 * POST: /articles - "persist new article"  
 * GET: /articles/create - To show the view to create an artile
 * 
 * 
 */
























// Route::get('/test', function () {
//     return view('test');
// });

// Route::get('/hello', function () {
//     // return ["name" => "Chandan","location"=>"us"];
//     // dd(request('name'));
//     $firstName = request('name');
    
//     return view('test',['name'=>$firstName]);

//     //view is what we seen on a webpage 
//     //view is kind of a php file returned as php
    
// });

// Route::get('/posts/{slug}','PostsController@show');